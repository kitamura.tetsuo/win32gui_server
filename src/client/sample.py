from stub import Stub

with Stub("localhost:65051") as stub:
    stub.set_title("タイトルなし - メモ帳")
    img = stub.capture_window()
    img.save("test.png")

    stub.click(10, 10)
