import io
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
sys.path.append(os.path.join(os.path.dirname(__file__), "..", "proto"))

import grpc
from PIL import Image

import proto.messages_pb2 as pb
import proto.messages_pb2_grpc as pb2_grpc


class Stub(object):
    def __init__(self, address) -> None:
        self.address = address

    def __enter__(self):
        self.channel = grpc.insecure_channel(self.address)
        self.stub = pb2_grpc.ServerStub(self.channel)
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.channel.close()

    def subprocess_popen(self, arg):
        self.stub.SubprocessPopen(pb.PopenRequest(arg=arg))

    def set_title(self, title):
        self.stub.SetTitle(pb.TitleRequest(title=title))

    def capture_window(self):
        response = self.stub.CaptureWindow(pb.Empty())
        stream = io.BytesIO(response.image)
        img = Image.open(stream)
        return img

    def click(self, x, y):
        self.stub.Click(pb.ClickRequest(x=x, y=y))

    def scroll(self, clicks, x, y):
        self.stub.Scroll(pb.ScrollRequest(clicks=clicks, x=x, y=y))
