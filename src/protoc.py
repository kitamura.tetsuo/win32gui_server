from grpc.tools import protoc

protoc.main(("", "-I.", "--python_out=./src/proto", "--grpc_python_out=./src/proto", "messages.proto"))
