import ctypes
import io
from typing import NamedTuple

import win32gui
from PIL import ImageGrab


class Point(NamedTuple):
    x: int
    y: int


class Capture:
    def __init__(self) -> None:
        self.bbox = None

    def find_window(self, window_name):
        hwnd = win32gui.FindWindow(None, window_name)
        if hwnd:
            self.bbox = win32gui.GetWindowRect(hwnd)
            f = ctypes.windll.dwmapi.DwmGetWindowAttribute
            rect = ctypes.wintypes.RECT()
            DWMWA_EXTENDED_FRAME_BOUNDS = 9
            f(
                ctypes.wintypes.HWND(hwnd),
                ctypes.wintypes.DWORD(DWMWA_EXTENDED_FRAME_BOUNDS),
                ctypes.byref(rect),
                ctypes.sizeof(rect),
            )
            self.bbox = (rect.left, rect.top, rect.right, rect.bottom)
            self.rect = rect

            return Point(rect.left, rect.top)

    def get_screenshot(self):
        if self.bbox is not None:
            return ImageGrab.grab(self.bbox)

    def get_screenshot_as_byte(self):
        img_bytes = io.BytesIO()
        self.get_screenshot().save(img_bytes, format="PNG")
        return img_bytes.getvalue()
