import os
import sys
import time

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
sys.path.append(os.path.join(os.path.dirname(__file__), "..", "proto"))


import subprocess
from concurrent import futures

import capture
import grpc
import pyautogui

import proto.messages_pb2 as pb
import proto.messages_pb2_grpc as pb2_grpc


class ServerService(pb2_grpc.ServerServicer):
    def __init__(self):
        self.capture = capture.Capture()

    def SetTitle(self, request: pb.TitleRequest, context):
        self.offset = self.capture.find_window(request.title)
        return pb.Empty()

    def SubprocessPopen(self, request, context):
        subprocess.Popen(request.arg, shell=True)
        return pb.Empty()

    def CaptureWindow(self, request, context):
        while True:
            try:
                return pb.CaptureWindowResponse(image=self.capture.get_screenshot_as_byte())
            except:
                pass

    def Click(self, click_request, context):
        pyautogui.click(x=click_request.x + self.offset.x, y=click_request.y + self.offset.y)
        return pb.Empty()

    def Drag(self, click_request, context):
        pyautogui.drag(click_request.x, click_request.y, duration=2, button="left")
        return pb.Empty()

    def Scroll(self, request, context):
        pyautogui.scroll(clicks=request.clicks, x=request.x + self.offset.x, y=request.y + self.offset.y)
        return pb.Empty()


# start server
server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
pb2_grpc.add_ServerServicer_to_server(ServerService(), server)
server.add_insecure_port("0.0.0.0:65051")
server.start()
print("run server")

# wait
try:
    while True:
        time.sleep(3600)
except KeyboardInterrupt:
    pass
finally:
    # stop server
    server.stop(0)
